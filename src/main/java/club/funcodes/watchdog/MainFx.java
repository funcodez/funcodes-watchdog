// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.refcodes.archetype.CliHelper;
import org.refcodes.checkerboard.ConsoleCheckerboardViewer;
import org.refcodes.checkerboard.NeighbourhoodPositions;
import org.refcodes.checkerboard.alt.javafx.FxCheckerboardViewer;
import org.refcodes.checkerboard.alt.javafx.FxChessboardFactory;
import org.refcodes.cli.ArgsFilter;
import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Option;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.component.InitializeException;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.ScaleMode;
import org.refcodes.graphical.ext.javafx.FxGraphicalUtility;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.Properties;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.VerboseTextBuilder;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Workaround for the FatJAR to work: Launch a <code>main()</code> method
 * which's class does *not* extend <code>Application</code> which then
 * statically calls this <code>MainFx</code>'s <code>main()</code> method.
 * 
 * "https://stackoverflow.com/questions/52653836/maven-shade-javafx-runtime-components-are-missing"
 */
public class MainFx extends Application {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "watchdog";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String TITLE = "WATCHDOG";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Credits [C] First Star Software, Inc. | Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/watchdog_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "Cellular automaton simulating a watchdog in a house using JavaFx (see [https://www.metacodes.pro/manpages/watchdog_manpage]).";

	private static final String FIELD_HEIGHT_PROPERTY = "fieldHeight";
	private static final String FIELD_WIDTH_PROPERTY = "fieldWidth";
	private static final String FIELD_GAP_PROPERTY = "fieldGap";
	private static final String BOARD_PROPERTY = "board";
	private static final String LIST_BOARDS_PROPERTY = "listBoards";
	private static final String CONSOLE_PROPERTY = "console";
	private static final String NEIGBOURHOOD_PROPERTY = "neighbourhood";
	private static final String SECTION_WATCHDOG = "watchdog";
	private static final String SECTION_BOARDS = "boards";

	private static final int FIELD_HEIGHT = 300;
	private static final int FIELD_WIDTH = 300;
	private static final int FIELD_GAP = 15;
	private static final String APPLICATION_ICON = "application.png";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {
		launch( args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start( Stage aPrimaryStage ) throws ArgsSyntaxException {
		final Option<Integer> theFieldWidthOption = intOption( null, "field-width", FIELD_WIDTH_PROPERTY, "The desired width of the checkerboard field's widths." );
		final Option<Integer> theFieldHeightOption = intOption( null, "field-height", FIELD_HEIGHT_PROPERTY, "The desired height of the checkerboard field's heights." );
		final Option<Integer> theFieldGapOption = intOption( null, "field-gap", FIELD_GAP_PROPERTY, "The desired gap between the checkerboard's fields." );
		final Flag theInitFlag = initFlag( false );
		final StringOption theConfigFileArg = configOption();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theVerboseArg = verboseFlag();
		final Flag theDebugArg = debugFlag();
		final Flag theConsoleArg = flag( null, "console", CONSOLE_PROPERTY, "Print the checkerboard also to the console." );
		final Flag theHelpArg = helpFlag( "Shows this help" );
		final Flag theListBoardsArg = flag( 'l', "list", LIST_BOARDS_PROPERTY, "Lists all boards available for the watchdog cellular automaton." );
		final Option<String> theBoardArg = stringOption( null, "board", BOARD_PROPERTY, "The board to be used for chosen cellular automaton." );
		final Option<NeighbourhoodPositions> theNeighbourhoodArg = enumOption( 'n', "neighbourhood", NeighbourhoodPositions.class, NEIGBOURHOOD_PROPERTY, "Use an according neighborhood, acceptable values are: " + new VerboseTextBuilder().toString( NeighbourhoodPositions.values() ) );

		// @formatter:off
		final Term theArgsSyntax = 	cases(
			and(
				theListBoardsArg,
				any (theVerboseArg, theDebugArg)
			),
			and(
				any(
					theNeighbourhoodArg, theBoardArg, theFieldWidthOption, theFieldHeightOption, theFieldGapOption, theConsoleArg, theConfigFileArg, theVerboseArg, theDebugArg
				)
			),
			and( theInitFlag, any( theConfigFileArg, theVerboseArg, theDebugArg ) ),
			xor( theHelpArg, and( theSysInfoFlag, optional( theVerboseArg ) ) )
		);

		final Example[] theExamples = examples(
			example("Launch random board simulation"),
			example("Launch random board simulation (verbose)", theVerboseArg ),
			example("List all boards for the simulation", theListBoardsArg ),
			example("List all boards for the simulation (verbose)", theListBoardsArg, theVerboseArg ),
			example("Launch random board simulation printing state to the console", theConsoleArg ),
			example("Start simulation for provided board", theBoardArg, theVerboseArg ),
			example("Start simulation for provided board (verbose)", theBoardArg, theVerboseArg ),
			example("Initialize a config file", theInitFlag, theConfigFileArg, theVerboseArg ),
			example("Start simulation from config file", theConfigFileArg, theVerboseArg ),
			example( "To show the help text", theHelpArg ),
			example( "To print the system info", theSysInfoFlag )
		);
		
		final String[] theArgs = getParameters().getRaw() != null ? getParameters().getRaw().toArray( new String[getParameters().getRaw().size()] ) : new String[] {};
		final CliHelper theCliHelper = CliHelper.builder().
			// withArgs( theArgs ).
			withArgs( theArgs, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( MainFx.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();
		final boolean isVerbose = theCliHelper.isVerbose();

		try {
			aPrimaryStage.getIcons().add( FxGraphicalUtility.toImage( Main.class.getResourceAsStream( "/" + APPLICATION_ICON ) ) );

			// ---------------------------------------------------------
			// Main:
			// ---------------------------------------------------------

			String theBoard = theArgsProperties.get( theBoardArg );
			boolean isList = theArgsProperties.getBoolean( theListBoardsArg );
			boolean isConsole = theArgsProperties.getBoolean( theConsoleArg );

			// WatchDog |-->

			Properties theProfile = theArgsProperties.toRuntimeProfile( SECTION_WATCHDOG );
			NeighbourhoodPositions theNeighbourhood = NeighbourhoodPositions.VON_NEUMANN;
			if ( NeighbourhoodPositions.VON_NEUMANN.name().equalsIgnoreCase( theProfile.get( NEIGBOURHOOD_PROPERTY ) ) ) {
				theNeighbourhood = NeighbourhoodPositions.VON_NEUMANN;
			}
			if ( NeighbourhoodPositions.MOORE.name().equalsIgnoreCase( theProfile.get( NEIGBOURHOOD_PROPERTY ) ) ) {
				theNeighbourhood = NeighbourhoodPositions.MOORE;
			}
			Integer theFieldWidth = theProfile.getInt( FIELD_WIDTH_PROPERTY );
			if ( theFieldWidth == null || theFieldWidth <= 0 ) {
				theFieldWidth = FIELD_WIDTH;
			}
			Integer theFieldHeight = theProfile.getInt( FIELD_HEIGHT_PROPERTY );
			if ( theFieldHeight == null || theFieldWidth <= 0 ) {
				theFieldHeight = FIELD_HEIGHT;
			}
			Integer theFieldGap = theProfile.getInt( FIELD_GAP_PROPERTY );
			if ( theFieldGap == null || theFieldWidth <= 0 ) {
				theFieldGap = FIELD_GAP;
			}
			initWatchDog( aPrimaryStage, theBoard, theNeighbourhood, theFieldWidth, theFieldHeight, theFieldGap, isList, theArgsProperties.retrieveFrom( theArgsProperties.toPath( SECTION_WATCHDOG, SECTION_BOARDS ) ), isConsole, isVerbose );

		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initializes the WatchDog cellular automaton.
	 * 
	 * @param aPrimaryStage The {@link Stage} to use from JavaFX.
	 * @param aBoard The alias for the WatchDog board to be used.
	 * @param aNeighbourhood The {@link Position} elements representing the
	 *        neighbourhood of a cell.
	 * @param aFieldWidth The width of a field.
	 * @param aFieldHeight The height of a field.
	 * @param aFieldGap The gap of a field.
	 * @param isList WHen true, then the available boards are listed, else the
	 *        automaton is started.
	 * @param isConsole When true, a console output is generated.
	 * @param isVerbose Be more verbose.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems. Usually a method similar to "initialize()"
	 *         throws such an exception.
	 */
	private void initWatchDog( Stage aPrimaryStage, String aBoard, NeighbourhoodPositions aNeighbourhood, Integer aFieldWidth, Integer aFieldHeight, Integer aFieldGap, boolean isList, Properties aBoards, boolean isConsole, boolean isVerbose ) throws IOException, InitializeException {
		// List |-->
		if ( isList ) {
			List<String> theList = new ArrayList<>( aBoards.dirs() );
			if ( theList.size() > 0 ) {
				Collections.sort( theList );
				if ( isVerbose ) {
					LOGGER.info( "Availability groundplans listing:" );
					LOGGER.printSeparator();
				}
				for ( String eGroundplan : theList ) {
					if ( isVerbose ) {
						LOGGER.info( eGroundplan );
					}
					else {
						System.out.println( eGroundplan );
					}
				}
				if ( isVerbose ) {
					LOGGER.printSeparator();
				}
			}
			else {
				String theMessage = "No WatchDog boards available. Make sure you define some in your configuration file '" + DEFAULT_CONFIG + ".*' below the section '" + SECTION_WATCHDOG + "/" + SECTION_BOARDS + "'.";
				if ( isVerbose ) {
					LOGGER.info( theMessage );
				}
				else {
					System.out.println( theMessage );
				}
			}
			LOGGER.printSeparator();
			System.exit( 0 );
		}
		// Automaton |-->
		else {
			String[] theGroundplan = null;
			if ( aBoard != null && aBoard.length() != 0 ) {
				theGroundplan = aBoards.getArray( aBoard );
				if ( theGroundplan == null || theGroundplan.length == 0 ) {
					throw new IllegalArgumentException( "You must provide a valid WatchDog board, but you provided <" + aBoard + ">." );
				}
			}
			else {
				List<String> theBoards = new ArrayList<>( aBoards.dirs() );
				aBoard = theBoards.get( new Random().nextInt( theBoards.size() ) );
				theGroundplan = aBoards.getArray( aBoard );
				if ( theGroundplan == null || theGroundplan.length == 0 ) {
					throw new IllegalArgumentException( "We encountered an invalid WatchDog board <" + aBoard + ">, please correct the configuration file '" + DEFAULT_CONFIG + ".*' below the section '" + SECTION_WATCHDOG + "/" + SECTION_BOARDS + "'." );
				}
			}

			aPrimaryStage.setTitle( aBoard.toUpperCase().replaceAll( "_", " " ) );
			GroundPlan theBoard = new GroundPlan( theGroundplan, aNeighbourhood.getNeighbours() );

			if ( isVerbose ) {
				LOGGER.info( "Board = " + aBoard );
				LOGGER.info( "Neighbourhood = " + aNeighbourhood );
				LOGGER.info( "Print to console = " + isConsole );
				LOGGER.info( "Width = " + theBoard.getGridWidth() );
				LOGGER.info( "Height = " + theBoard.getGridHeight() );
			}

			FxChessboardFactory theBackgroundFactory = new FxChessboardFactory().withFieldGapColor( Color.rgb( 0xff, 0xff, 0xff ) ).withOddFieldColor( Color.rgb( 0xc4, 0xed, 0xd7 ) ).withEvenFieldColor( Color.rgb( 0xab, 0xd2, 0xc8 ) );
			WatchdogFxSpriteFactory theSpriteFactory = new WatchdogFxSpriteFactory().withScaleFactor( 1 );

			FxCheckerboardViewer<WatchdogPlayer, WatchdogStatus> theJavaFxViewer = new FxCheckerboardViewer<WatchdogPlayer, WatchdogStatus>( theBoard );
			theJavaFxViewer.withSpriteFactory( theSpriteFactory ).withBackgroundFactory( theBackgroundFactory );
			theJavaFxViewer.withViewportDimension( theBoard.getGridWidth(), theBoard.getGridHeight() );
			theJavaFxViewer.withMovePlayerDurationMillis( 5 ).withRemovePlayerDurationMillis( 0 ).withAddPlayerDurationMillis( 0 );
			theJavaFxViewer.withDragOpacity( 0.9 ).withMoveMode( MoveMode.SMOOTH ).withScaleMode( ScaleMode.NONE );
			theJavaFxViewer.withFieldDimension( aFieldWidth, aFieldHeight, aFieldGap );
			theJavaFxViewer.initialize();

			Scene theScene = new Scene( theJavaFxViewer, theJavaFxViewer.getContainerWidth(), theJavaFxViewer.getContainerHeight(), Color.GREEN );
			aPrimaryStage.setScene( theScene );
			aPrimaryStage.sizeToScene();
			aPrimaryStage.setResizable( false );
			aPrimaryStage.show();

			//	FxChessboardFactory<WatchdogPlayer, WatchdogStatus> theFactory = new FxChessboardFactoryImpl<WatchdogPlayer, WatchdogStatus>().withFieldGapColor( Color.rgb( 0xff, 0xff, 0xff ) ).withOddFieldColor( Color.rgb( 0xc4, 0xed, 0xd7 ) ).withEvenFieldColor( Color.rgb( 0xab, 0xd2, 0xc8 ) );
			//	FxCheckerboardViewer<WatchdogPlayer, WatchdogStatus> theJavaFxViewer = new FxCheckerboardViewerImpl<WatchdogPlayer, WatchdogStatus>( theBoard, aPrimaryStage );
			//	theJavaFxViewer.withSpriteFactory( new WatchdogFxSpriteFactory().withScaleFactor( 1 ) );
			//	theJavaFxViewer.withDragOpacity( 0.9 ).withFieldDimension( aFieldWidth, aFieldHeight, aFieldGap );
			//	theJavaFxViewer.withRemovePlayerDurationMillis( 0 ).withAddPlayerDurationMillis( 0 );
			//	theJavaFxViewer.withBackgroundFactory( theFactory );
			//	theJavaFxViewer.withScaleMode( ScaleMode.NONE ).withInitialize().show();

			if ( isConsole ) {
				if ( isVerbose ) {
					LOGGER.printTail();
				}
				ConsoleCheckerboardViewer<WatchdogPlayer, WatchdogStatus> theConsoleViewer = new ConsoleCheckerboardViewer<WatchdogPlayer, WatchdogStatus>( theBoard, new WatchdogConsoleSpriteFactory() );
				theConsoleViewer.withColumnWidth( 6 ).initialize();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() throws Exception {
		System.exit( 0 );
	}
}
