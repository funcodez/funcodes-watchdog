// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

/**
 * The {@link WatchdogMode} represents the states a IWuff can be in.
 */
public enum WatchdogMode {

	ACTIVE('a'), ACTIVE_ACTIVITY('A'), INACTIVE('i'), INACTIVE_ACTIVITY('I'), NONE(' ');

	private char _statusId;

	private WatchdogMode( char aStatisId ) {
		_statusId = aStatisId;
	}

	/**
	 * Returns the TID of the given {@link WatchdogMode}.
	 * 
	 * @return TRhe according TID.
	 */
	public char getAsciiChar() {
		return _statusId;
	}

	/**
	 * Converts the state TID to a {@link WatchdogMode}.
	 * 
	 * @param aStatusId
	 * 
	 * @return
	 */
	public static WatchdogMode toStruppieStatus( char aStatusId ) {
		for ( WatchdogMode eStatus : values() ) {
			if ( eStatus.getAsciiChar() == aStatusId ) {
				return eStatus;
			}
		}
		return null;
	}

	/**
	 * Returns true in case the {@link WatchdogMode} {@link WatchdogMode#ACTIVE}
	 * or {@link WatchdogMode#ACTIVE_ACTIVITY}.
	 * 
	 * @return True in case our IWuff is active and in turn is the one who has
	 *         the bone.
	 */
	public boolean hasBone() {
		return this == WatchdogMode.ACTIVE || this == WatchdogMode.ACTIVE_ACTIVITY;
	}
}
