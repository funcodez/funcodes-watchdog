// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import org.refcodes.checkerboard.ConsoleCheckerboardViewer;
import org.refcodes.checkerboard.ConsoleSpriteFactory;
import org.refcodes.checkerboard.SpriteFactory;

/**
 * Implementation of the {@link SpriteFactory} interface
 * ({@link ConsoleSpriteFactory}) for the WatchdogPlayer simulator.
 */
public class WatchdogConsoleSpriteFactory implements ConsoleSpriteFactory<WatchdogStatus> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String create( WatchdogStatus aIdentifier, ConsoleCheckerboardViewer<?, WatchdogStatus> aContext ) {
		int theHopCount = aIdentifier.getHopCount();
		return aIdentifier.getWatchdogMode().getAsciiChar() + "<" + ( ( theHopCount == -1 ) ? "?" : theHopCount ) + ">";
	}
}
