// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import org.refcodes.checkerboard.GraphicalPlayerState;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.Observer;

/**
 * The status of WatchdogPlayer including its {@link WatchdogMode} and the
 * hop-count to the nearest activity.
 */
public class WatchdogStatus extends AbstractObservable<Observer<WatchdogStatusEvent>, WatchdogStatusEvent> implements GraphicalPlayerState {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	static final int HOP_COUNT_EXPIRATION_MILLIS = 15000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private WatchdogPlayer _watchDogPlayer;
	private WatchdogMode _watchdogMode;
	private int _hopCount;
	private long _hopCountTimestamp = -1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link WatchdogStatus}, being a combination of hop-count
	 * and {@link WatchdogMode}.
	 * 
	 * @param aWatchDogPlayer The {@link WatchdogPlayer} to which this
	 *        {@link WatchdogStatus} belongs.
	 * @param aWatchdogMode The {@link WatchdogMode} to be set.
	 * @param aHopCount The hop-count to be set.
	 */
	public WatchdogStatus( WatchdogPlayer aWatchDogPlayer, WatchdogMode aWatchdogMode, int aHopCount ) {
		_watchDogPlayer = aWatchDogPlayer;
		setWatchdogMode( aWatchdogMode );
		setHopCount( aHopCount );
	}

	/**
	 * Constructs the {@link WatchdogStatus}, being a combination of hop-count
	 * and {@link WatchdogMode}.
	 * 
	 * @param aWatchDogPlayer The {@link WatchdogPlayer} to which this
	 *        {@link WatchdogStatus} belongs.
	 * @param aWatchdogMode The {@link WatchdogMode} to be set.
	 * @param aHopCount The hop-count to be set.
	 * @param aHopCountTimeStamp The timestamp to be set for the loast hop-count
	 *        update.
	 */
	private WatchdogStatus( WatchdogPlayer aWatchDogPlayer, WatchdogMode aWatchdogMode, int aHopCount, long aHopCountTimeStamp ) {
		this( aWatchDogPlayer, aWatchdogMode, aHopCount );
		_hopCountTimestamp = aHopCountTimeStamp;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets WatchdogPlayer's activity.
	 * 
	 * @param aWatchdogMode The {@link WatchdogMode} to be used.
	 */
	public void setWatchdogMode( WatchdogMode aWatchdogMode ) {
		WatchdogMode thePrevMode = _watchdogMode;
		_watchdogMode = aWatchdogMode;
		onWatchdogStatusChanged( thePrevMode, _watchdogMode );
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * provided new {@link WatchdogStatus} and the current hop-count.
	 * 
	 * @param aWatchdogMode The {@link WatchdogMode} to be used.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public WatchdogStatus toWatchdogMode( WatchdogMode aWatchdogMode ) {
		return new WatchdogStatus( _watchDogPlayer, aWatchdogMode, getHopCount(), _hopCountTimestamp );
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and the provided hop-count.
	 * 
	 * @param aHopCount The hop-count to be set.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public void setHopCount( int aHopCount ) {
		int thePrevHopCount = _hopCount;
		_hopCount = aHopCount;
		_hopCountTimestamp = System.currentTimeMillis();
		onHopCountChanged( thePrevHopCount, _hopCount );
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and a hop-count of 0 signaling the
	 * activity is "here" at this {@link WatchdogPlayer}.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public WatchdogStatus toHereHopCount() {
		return new WatchdogStatus( _watchDogPlayer, getWatchdogMode(), 0, System.currentTimeMillis() );
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and the provided hop-count.
	 * 
	 * @param aHopCount The hop-count to be set.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	//	public WatchdogStatus toHopCount( int aHopCount ) {
	//		return new WatchdogStatus( _watchDogPlayer, getWatchdogMode(), aHopCount, System.currentTimeMillis() );
	//	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and the provided hop-count incremented by
	 * 1.
	 * 
	 * @param aHopCount The hop-count incremented by 1 to be set.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public WatchdogStatus toIncrementHopCount( int aHopCount ) {
		return new WatchdogStatus( _watchDogPlayer, getWatchdogMode(), aHopCount + 1, System.currentTimeMillis() );
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and the hop-count increased by one.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public void incrementHopCount() {
		setHopCount( _hopCount + 1 );
		_hopCountTimestamp = System.currentTimeMillis();
	}

	/**
	 * Constructs the {@link WatchdogStatus} from this instance using the
	 * current {@link WatchdogStatus} and the hop-count increased by one.
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public WatchdogStatus toIncrementHopCount() {
		return new WatchdogStatus( _watchDogPlayer, getWatchdogMode(), getHopCount() + 1, System.currentTimeMillis() );
	}

	/**
	 * Invalidates the hop-count (being set to -1).
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public void invalidateHopCount() {
		setHopCount( -1 );
		_hopCountTimestamp = System.currentTimeMillis();
	}

	/**
	 * Invalidates the hop-count (being set to -1).
	 * 
	 * @return The resulting {@link WatchdogStatus}.
	 */
	public WatchdogStatus toInvalidateHopCount() {
		return new WatchdogStatus( _watchDogPlayer, getWatchdogMode(), -1, System.currentTimeMillis() );
	}

	/**
	 * Determines whether the hop-count has expired and can be taken from
	 * overseas.
	 * 
	 * @return True in case the hop cpount has expired (is invalid now).
	 */
	public boolean isHopCountExpired() {
		return _hopCountTimestamp == -1 || _hopCountTimestamp + HOP_COUNT_EXPIRATION_MILLIS < System.currentTimeMillis();
	}

	/**
	 * Returns the current {@link WatchdogMode} of {@link WatchdogBrain}.
	 * 
	 * @return The according {@link WatchdogMode}.
	 */
	public WatchdogMode getWatchdogMode() {
		return _watchdogMode;
	}

	/**
	 * Returns the hop-count distance to the origin of activity.
	 * 
	 * @return the according hop-count.
	 */
	public int getHopCount() {
		return _hopCount;
	}

	/**
	 * Returns the {@link WatchdogPlayer} to which this {@link WatchdogStatus}
	 * belongs.
	 * 
	 * @return The according {@link WatchdogPlayer}.
	 */
	public WatchdogPlayer getWatchdogPlayer() {
		return _watchDogPlayer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "WatchdogStatus [watchdogMode=" + _watchdogMode + ", hopCount=" + _hopCount + "]";
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Hook being called upon a hop-count update.
	 * 
	 * @param aPrevHopCount The previous hop-count.
	 * @param aNewHopCount The new (current) hop-count.
	 */
	protected void onHopCountChanged( int aPrevHopCount, int aNewHopCount ) {
		try {
			fireEvent( new WatchdogStatusEvent( this ), ExecutionStrategy.SEQUENTIAL );
		}
		catch ( VetoException ignore ) {}
	}

	/**
	 * Hook being called upon a {@link WatchdogMode} update.
	 * 
	 * @param aPrevMode The previous {@link WatchdogMode}.
	 * @param aNewMode The new (current) {@link WatchdogMode}.
	 */
	protected void onWatchdogStatusChanged( WatchdogMode aPrevMode, WatchdogMode aNewMode ) {
		try {
			fireEvent( new WatchdogStatusEvent( this ), ExecutionStrategy.SEQUENTIAL );
		}
		catch ( VetoException ignore ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected boolean fireEvent( WatchdogStatusEvent aEvent, Observer<WatchdogStatusEvent> aObserver, ExecutionStrategy aExecutionStrategy ) throws Exception {
		aObserver.onEvent( aEvent );
		return true;
	}
}
