// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.refcodes.checkerboard.AbstractPlayer;
import org.refcodes.checkerboard.Player;
import org.refcodes.checkerboard.VonNeumannNeighbourhood;
import org.refcodes.component.Configurable;
import org.refcodes.component.ConfigureException;
import org.refcodes.component.Destroyable;
import org.refcodes.graphical.Position;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;

public class WatchdogPlayer extends AbstractPlayer<WatchdogPlayer, WatchdogStatus> implements WatchdogBrain, Player<WatchdogPlayer, WatchdogStatus>, Configurable<GroundPlan>, Destroyable {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int ACTIVITY_DURATION_MILLIS = 400;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private GroundPlan _context;
	private Position[] _neighbourhoods;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the plain instance.
	 */
	public WatchdogPlayer() {
		this( WatchdogMode.NONE, 0, 0, -1, VonNeumannNeighbourhood.values() );
	}

	/**
	 * Constructs the instance with the given coordinates.
	 * 
	 * @param aStruppieMode The according {@link WatchdogMode}.
	 * @param aPosX The x-position for the instance being created.
	 * @param aPosY The y-position for the instance being created.
	 */
	public WatchdogPlayer( WatchdogMode aStruppieMode, int aPosX, int aPosY ) {
		this( aStruppieMode, aPosX, aPosY, -1, VonNeumannNeighbourhood.values() );
	}

	/**
	 * Constructs the instance with the given coordinates.
	 * 
	 * @param aStruppieMode The according {@link WatchdogMode}.
	 * @param aPosX The x-position for the instance being created.
	 * @param aPosY The y-position for the instance being created.
	 * @param aNeighbourhoods The {@link Neighbours} of a cell.
	 */
	public WatchdogPlayer( WatchdogMode aStruppieMode, int aPosX, int aPosY, Position[] aNeighbourhoods ) {
		this( aStruppieMode, aPosX, aPosY, -1, aNeighbourhoods );
	}

	/**
	 * Constructs the instance with the given coordinates.
	 * 
	 * @param aStruppieMode The according {@link WatchdogMode}.
	 * @param aPosX The x-position for the instance being created.
	 * @param aPosY The y-position for the instance being created.
	 * @param aHopCount The hop-count till the nearest activity.
	 */
	public WatchdogPlayer( WatchdogMode aStruppieMode, int aPosX, int aPosY, int aHopCount ) {
		this( aStruppieMode, aPosX, aPosY, aHopCount, VonNeumannNeighbourhood.values() );
	}

	/**
	 * Constructs the instance with the given coordinates.
	 * 
	 * @param aStruppieMode The according {@link WatchdogMode}.
	 * @param aPosX The x-position for the instance being created.
	 * @param aPosY The y-position for the instance being created.
	 * @param aHopCount The hop-count till the nearest activity.
	 * @param aNeighbourhoods The {@link Neighbours} of a cell.
	 */
	public WatchdogPlayer( WatchdogMode aStruppieMode, int aPosX, int aPosY, int aHopCount, Position[] aNeighbourhoods ) {
		super( aPosX, aPosY );
		setStatus( new WatchdogStatus( this, aStruppieMode, aHopCount ) );
		Thread t = new Thread( this::checkStruppieStateDaemon );
		_neighbourhoods = aNeighbourhoods;
		t.setDaemon( true );
		t.start();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void onActivity() {
		for ( WatchdogPlayer eStruppie : _context.getPlayers() ) {
			eStruppie.cease();
		}
		if ( getStatus().getWatchdogMode() == WatchdogMode.INACTIVE ) {
			if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
				setStatus( getStatus().toWatchdogMode( WatchdogMode.INACTIVE_ACTIVITY ).toHereHopCount() );
			}
			else {
				getStatus().setHopCount( 0 );
				getStatus().setWatchdogMode( WatchdogMode.INACTIVE_ACTIVITY );
			}
		}
		Position ePos;
		for ( Position eNeighbour : _neighbourhoods ) {
			ePos = eNeighbour.toAddTo( this );
			WatchdogPlayer eStruppie = _context.atPosition( ePos );
			if ( eStruppie != null ) {
				new Thread( () -> eStruppie.onActivity( this ) ).start();
			}
		}
	}

	/**
	 * Reset any hop-count.
	 */
	public void cease() {
		getStatus().invalidateHopCount();
	}

	/**
	 * Hook for getting notified when a player's activity changed.
	 * 
	 * @param aPlayer The player which's activity changed.
	 */
	public synchronized void onActivity( WatchdogPlayer aPlayer ) {
		int eHopCount = aPlayer.getStatus().getHopCount();
		int theHopCount = getStatus().isHopCountExpired() ? -1 : getStatus().getHopCount();
		if ( theHopCount == -1 || theHopCount > eHopCount + 1 ) {
			Position ePos;
			if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
				setStatus( getStatus().toIncrementHopCount( eHopCount ) );
			}
			else {
				getStatus().setHopCount( eHopCount + 1 );
			}
			for ( Position eNeighbour : _neighbourhoods ) {
				ePos = eNeighbour.toAddTo( this );
				WatchdogPlayer eStruppie = _context.atPosition( ePos );
				if ( eStruppie != null && eStruppie != aPlayer ) {
					new Thread( () -> eStruppie.onActivity( this ) ).start();
				}
			}
		}
		if ( getStatus().getWatchdogMode().hasBone() ) {
			catchBone();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void catchBone() {
		if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
			setStatus( getStatus().toWatchdogMode( WatchdogMode.ACTIVE_ACTIVITY ) );
		}
		else {
			getStatus().setWatchdogMode( WatchdogMode.ACTIVE_ACTIVITY );
		}

		try {
			Thread.sleep( ACTIVITY_DURATION_MILLIS );
		}
		catch ( InterruptedException ignore ) {}

		int theHopCount = getStatus().isHopCountExpired() ? -1 : getStatus().getHopCount();

		if ( theHopCount != 0 ) {
			Position ePos;
			WatchdogPlayer theStruppie = this;
			WatchdogPlayer eStruppie;
			int eHopCount;
			List<Position> theNeighbours = new ArrayList<>( Arrays.asList( _neighbourhoods ) );
			Collections.shuffle( theNeighbours );
			for ( Position eNeighbour : theNeighbours ) {
				ePos = eNeighbour.toAddTo( this );
				eStruppie = _context.atPosition( ePos );
				if ( eStruppie != null ) {
					eHopCount = eStruppie.getStatus().getHopCount();
					if ( eHopCount < theHopCount && ( theStruppie == null || eHopCount < theHopCount ) ) {
						theStruppie = eStruppie;
						theHopCount = eHopCount;
					}
				}
			}
			if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
				setStatus( getStatus().toWatchdogMode( WatchdogMode.INACTIVE ) );
			}
			else {
				getStatus().setWatchdogMode( WatchdogMode.INACTIVE );
			}

			final WatchdogPlayer theFetcher = theStruppie;
			new Thread( () -> theFetcher.catchBone() ).start();

		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize( GroundPlan aContext ) throws ConfigureException {
		LOGGER.debug( "Initializing WatchdogPlayer<" + toString() + "> ..." );
		_context = aContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {}

	/**
	 * Loops the playground printing via {@link #printPlayground()}.
	 */
	private void checkStruppieStateDaemon() {
		while ( true ) {
			if ( getStatus().isHopCountExpired() ) {
				switch ( getStatus().getWatchdogMode() ) {
				case ACTIVE:
					break;
				case ACTIVE_ACTIVITY:
					if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
						setStatus( getStatus().toWatchdogMode( WatchdogMode.ACTIVE ) );
					}
					else {
						getStatus().setWatchdogMode( WatchdogMode.ACTIVE );
					}
					break;
				case INACTIVE:
					break;
				case INACTIVE_ACTIVITY:
					if ( WatchdogFxSpriteFactory.IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
						setStatus( getStatus().toWatchdogMode( WatchdogMode.INACTIVE ) );
					}
					else {
						getStatus().setWatchdogMode( WatchdogMode.INACTIVE );
					}
					break;
				case NONE:
					break;
				default:
					break;
				}
			}
			try {
				Thread.sleep( WatchdogStatus.HOP_COUNT_EXPIRATION_MILLIS / 2 );
			}
			catch ( InterruptedException ignore ) {}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Simulate noise on which {@link WatchdogPlayer} is to wake up.
	 * {@inheritDoc}
	 */
	@Override
	public void click() {
		super.click();
		if ( getStatus().getHopCount() != 0 ) {
			new Thread( this::onActivity ).start();
		}
	}
}
