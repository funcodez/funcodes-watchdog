module club.funcodes.watchdog {
	requires org.refcodes.archetype;
	requires org.refcodes.checkerboard.alt.javafx;
	requires org.refcodes.cli;
	requires org.refcodes.component;
	requires org.refcodes.exception;
	requires org.refcodes.factory;
	requires org.refcodes.graphical;
	requires org.refcodes.logger;
	requires org.refcodes.logger.alt.console;
	requires transitive org.refcodes.checkerboard;
	requires transitive javafx.graphics;

	exports club.funcodes.watchdog;
}
