// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import org.refcodes.checkerboard.AbstractCheckerboard;
import org.refcodes.checkerboard.Checkerboard;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.Position;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;

/**
 * WatchdogPlayer's playground ({@link Checkerboard}).
 */
public class GroundPlan extends AbstractCheckerboard<GroundPlan, WatchdogPlayer, WatchdogStatus> {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an empty {@link GroundPlan}.
	 */
	public GroundPlan() {
		setGridMode( GridMode.CLOSED );
	}

	/**
	 * Constructs a {@link GroundPlan} from, the given ASCII map.
	 * 
	 * @param aGroundPlan The ASCII map for initializing the {@link GroundPlan}.
	 *        * @param aNeighbourhoods The neighbors to be used for a cell (e.g.
	 *        Von Neumann or Moore neighbourhood).
	 */
	public GroundPlan( String[] aGroundPlan, Position[] aNeighbourhoods ) {
		this();
		int theWidth = 0;
		int theHeight = aGroundPlan.length;
		if ( theHeight != 0 ) {
			theWidth = aGroundPlan[0].length();
		}
		else {
			throw new IllegalArgumentException( "Illegal width and height <" + theWidth + ", " + theHeight + " of the provided groundplan." );

		}
		LOGGER.debug( "Using groundplan width <" + theWidth + "> and height <" + theHeight + ">" );
		setGridDimension( theWidth, theHeight );
		String eLine;
		char eChar;
		WatchdogPlayer ePlayer = null;
		for ( int y = 0; y < theHeight; y++ ) {
			eLine = aGroundPlan[y];
			for ( int x = 0; x < eLine.length(); x++ ) {
				eChar = eLine.charAt( x );
				if ( eChar != ' ' && eChar != '#' ) {
					ePlayer = new WatchdogPlayer( WatchdogMode.toStruppieStatus( eChar ), x, y, aNeighbourhoods ).withDraggable( false );
					ePlayer.initializeUnchecked( this );
					putPlayer( ePlayer );
					ePlayer.onClicked( evt -> LOGGER.debug( "Click detected: " + evt.getSource() ) );
				}
			}
		}
	}
}
