// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import org.refcodes.mixin.StatusAccessor;

/**
 * The brain of Struppie.
 */
public interface WatchdogBrain extends StatusAccessor<WatchdogStatus> {

	/**
	 * Struppie should take the bone, bark, and set its hop-count to zero. Also
	 * all neighbours are to be notified on its hop-count and status change
	 */
	void catchBone();

	/**
	 * Struppie hears noises really nearby and should react according to its
	 * status and set its hop-count to zero. Also all neighbours are to be
	 * notified on its hop-count and status change
	 */
	void onActivity();

	/**
	 * Somme other {@link WatchdogBrain} noticed some noise either directly or
	 * somewhere further away,
	 * 
	 * @param aPlayer
	 */
	void onActivity( WatchdogPlayer aPlayer );

	/**
	 * Returns the current {@link WatchdogMode} of the {@link WatchdogBrain}.
	 * 
	 * @return The according {@link WatchdogMode}.
	 */
	default WatchdogMode getStruppieMode() {
		return getStatus().getWatchdogMode();
	}

	/**
	 * Returns the hop-count distance to the origin of activity.
	 * 
	 * @return the according hop-count.
	 */
	default int getHopCount() {
		return getStatus().getHopCount();
	}
}
