// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.watchdog;

import java.util.Map;
import java.util.WeakHashMap;

import org.refcodes.checkerboard.CheckerboardViewer;
import org.refcodes.checkerboard.SpriteFactory;
import org.refcodes.checkerboard.alt.javafx.AbstractFxSpriteFactory;
import org.refcodes.checkerboard.alt.javafx.FxCheckerboardViewer;
import org.refcodes.checkerboard.alt.javafx.FxSpriteFactory;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.graphical.ext.javafx.FxGraphicalUtility;
import org.refcodes.graphical.ext.javafx.FxLabelDecorator;
import org.refcodes.observer.Observer;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.VertAlignTextMode;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * Implementation of the {@link SpriteFactory} interface
 * ({@link FxSpriteFactory}) for the WatchdogPlayer simulator.
 */
public class WatchdogFxSpriteFactory extends AbstractFxSpriteFactory<WatchdogStatus, WatchdogFxSpriteFactory> implements FxSpriteFactory<WatchdogStatus> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * There are many ways to achieve a goal:
	 */
	static boolean IS_STATE_CHANGE_ON_SAME_INSTANCE = true;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static Image _activeImage = FxGraphicalUtility.toImage( WatchdogStatusNode.class.getResourceAsStream( "/Active.png" ) );
	private static Image _inactiveActivityImage = FxGraphicalUtility.toImage( WatchdogStatusNode.class.getResourceAsStream( "/Inactive_Activity.png" ) );
	private static Image _inactiveImage = FxGraphicalUtility.toImage( WatchdogStatusNode.class.getResourceAsStream( "/Inactive.png" ) );
	private static Image _activeActivityImage = FxGraphicalUtility.toImage( WatchdogStatusNode.class.getResourceAsStream( "/Active_Activity.png" ) );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Map<CheckerboardViewer<?, ?, ?>, Map<WatchdogPlayer, WatchdogStatusNode>> _checkerBoardToPlayerToNode = new WeakHashMap<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new fx boulder dash factory impl.
	 */
	public WatchdogFxSpriteFactory() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Node create( WatchdogStatus aIdentifier, FxCheckerboardViewer<?, WatchdogStatus> aContext ) {
		WatchdogStatusNode theNode = null;
		if ( IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
			WatchdogPlayer theWatchdogPlayer = aIdentifier.getWatchdogPlayer();
			Map<WatchdogPlayer, WatchdogStatusNode> theMap = _checkerBoardToPlayerToNode.get( aContext );
			if ( theMap == null ) {
				theMap = new WeakHashMap<>();
				_checkerBoardToPlayerToNode.put( aContext, theMap );
			}
			else {
				theNode = theMap.get( theWatchdogPlayer );
			}
			if ( theNode == null ) {
				theNode = new WatchdogStatusNode( aIdentifier, aContext );
				theNode = toInitNode( theNode, aContext );
				theMap.put( theWatchdogPlayer, theNode );
			}
			else {
				theNode.updateWatchdogStatus( aIdentifier );
			}
			theNode.setOpacity( getOpacity() );
			theNode.setVisible( true );
			return theNode;
		}
		else {
			theNode = new WatchdogStatusNode( aIdentifier, aContext );
			return toInitNode( theNode, aContext );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	//	private static Node toLabel( WatchdogStatus aStruppieStatus ) {
	//		FxLabelDecorator theCell = new FxLabelDecorator( "WUFF!", "Dialog", 0.25, toImage( aStruppieStatus.getStruppieMode() ) );
	//		theCell.setHorizAlignTextMode( HorizAlignTextMode.LEFT );
	//		theCell.setVertAlignTextMode( VertAlignTextMode.TOP );
	//		theCell.setLeftTextPaddingFactor( 0.03 );
	//		theCell.setTopTextPaddingFactor( 0.0 );
	//		theCell.setRightTextPaddingFactor( 0.025 );
	//		theCell.setBottomTextPaddingFactor( 0.025 );
	//		theCell.setTextMarginFactor( 0.05 );
	//		theCell.setTextBorderArcFactor( 0.05 );
	//		theCell.setTextBackground( Color.YELLOW );
	//		theCell.setTextColor( Color.RED );
	//		return theCell;
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Delegates any status changes of the {@link WatchdogStatus} instance to
	 * the provided {@link FxLabelDecorator} node.
	 */
	private class WatchdogStatusNode extends FxLabelDecorator implements Observer<WatchdogStatusEvent> {

		/**
		 * Constructs the {@link WatchdogStatusNode}.
		 * 
		 * @param aWatchdogStatus The status on which to listen.
		 * @param aContext The context in which the {@link WatchdogStatusNode}
		 *        will be used.
		 */
		public WatchdogStatusNode( WatchdogStatus aWatchdogStatus, FxCheckerboardViewer<?, WatchdogStatus> aContext ) {
			// super( toHopCountText( aStruppieStatus.getHopCount() ), "Dialog", 0.25, toLabel( aStruppieStatus ) );
			super( toHopCountText( aWatchdogStatus.getHopCount() ), "Dialog", 0.25, toImage( aWatchdogStatus.getWatchdogMode() ) );
			setPrefWidth( aContext.getFieldWidth() );
			setPrefHeight( aContext.getFieldHeight() );
			setHorizAlignTextMode( HorizAlignTextMode.RIGHT );
			setVertAlignTextMode( VertAlignTextMode.BOTTOM );
			setLeftTextPaddingFactor( 0.03 );
			setTopTextPaddingFactor( 0.0 );
			setRightTextPaddingFactor( 0.025 );
			setBottomTextPaddingFactor( 0.025 );
			setTextMarginFactor( 0.05 );
			setTextBorderArcFactor( 0.05 );
			setTextBackground( Color.WHITE );
			setTextColor( Color.BLACK );
			if ( !IS_STATE_CHANGE_ON_SAME_INSTANCE ) {
				aWatchdogStatus.subscribeObserver( this );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onEvent( WatchdogStatusEvent aEvent ) {
			updateWatchdogStatus( aEvent.getStatus() );
		}

		/**
		 * Updates the {@link WatchdogStatus}.
		 * 
		 * @param aStatus The status from which to get the parameters to update.
		 */
		void updateWatchdogStatus( WatchdogStatus aStatus ) {
			setImage( toImage( aStatus.getWatchdogMode() ) );
			setText( toHopCountText( aStatus.getHopCount() ) );
		}
	}

	/**
	 * Converts the hop-count to a text, ehttps://www.metacodes.proly handling
	 * the -1 hop-count,
	 * 
	 * @param aHopCount The hop-count for which to generate a text.
	 * 
	 * @return The according hop-count.
	 */
	private static String toHopCountText( int aHopCount ) {
		return aHopCount == -1 ? "?" : "" + aHopCount;
	}

	/**
	 * Resolves the {@link Image} from the {@link WatchdogMode}.
	 * 
	 * @param aStruppieMode The mode for which to resolve the image.
	 * 
	 * @return The according image.
	 */
	private static Image toImage( WatchdogMode aStruppieMode ) {
		switch ( aStruppieMode ) {
		case ACTIVE: {
			return _activeImage;
		}
		case INACTIVE_ACTIVITY: {
			return _inactiveActivityImage;
		}
		case INACTIVE: {
			return _inactiveImage;
		}
		case ACTIVE_ACTIVITY: {
			return _activeActivityImage;
		}
		default: {
			throw new UnhandledEnumBugException( aStruppieMode );
		}
		}
	}
}
